import { EntityModel } from "./EntityModel";
import { MilestoneEntity } from "./Milestone";

export interface TaskEntity extends EntityModel {
    readonly id: number;
    readonly name: string;
    readonly milestones?: MilestoneEntity[];
}

export interface TaskCreation extends EntityModel {
    readonly name: string;
}
