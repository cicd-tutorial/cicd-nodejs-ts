import { EntityModel } from "./EntityModel";

export interface PostTagCreation extends EntityModel {
    readonly postId: number;
    readonly tagId: number;
}
