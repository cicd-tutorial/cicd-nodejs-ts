import { EntityModel } from "./EntityModel";

export interface PostEntityTag extends EntityModel {
    readonly id: number;
    readonly name: string; 
}

export interface PostEntity extends EntityModel {
    readonly id: number;
    readonly name: string;
    readonly tags?: PostEntityTag[];
}

export interface PostCreation extends EntityModel {
    readonly name: string;
}
