import { EntityModel } from "./EntityModel";

export interface MilestoneEntity extends EntityModel {
    id: number;
    taskId: number;
    name: string;
}

export interface MilestoneCreation extends EntityModel {
    taskId: number;
    name: string;
}
