import { EntityModel } from "./EntityModel";

export interface TagEntityPost extends EntityModel {
    readonly id: number;
    readonly name: string;
}

export interface TagEntity extends EntityModel {
    readonly id: number;
    readonly name: string;
    readonly posts?: TagEntityPost[];
}

export interface TagCreation extends EntityModel {
    readonly name: string;
}
