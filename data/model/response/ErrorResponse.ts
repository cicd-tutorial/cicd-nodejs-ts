import ResponseModel from "./ResponseModel";

export default interface ErrorResponse extends ResponseModel {
    readonly code: number;
    readonly reason: string;
    readonly message: string;
}
