import ResponseModel from "./ResponseModel";

export default interface MilestoneResponse extends ResponseModel {
    readonly id: number;
    readonly taskId: number;
    readonly name: string;
}
