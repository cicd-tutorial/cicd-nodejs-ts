import ResponseModel from "./ResponseModel";

export default interface MessageResponse extends ResponseModel {
    readonly message: string;
}
