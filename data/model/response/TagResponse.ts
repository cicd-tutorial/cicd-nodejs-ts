import ResponseModel from "./ResponseModel";

export default interface TagResponse extends ResponseModel {
    readonly id: number;
    readonly name: string;
}
