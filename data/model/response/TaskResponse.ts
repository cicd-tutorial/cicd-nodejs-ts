import ResponseModel from "./ResponseModel";

export interface TaskResponseMilestone extends ResponseModel {
    readonly id: number;
    readonly name: string;
}

export default interface TaskResponse extends ResponseModel {
    readonly id: number;
    readonly name: string;
    readonly milestones?: TaskResponseMilestone[];
}
