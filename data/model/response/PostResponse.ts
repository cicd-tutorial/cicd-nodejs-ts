import ResponseModel from "./ResponseModel";
import TagResponse from "./TagResponse";

export default interface PostResponse extends ResponseModel {
    readonly id: number;
    readonly name: string;
    readonly tags?: TagResponse[];
}
