import ResponseModel from "../response/ResponseModel";

export interface ResponseWithStatus {
    readonly status: number;
    readonly response: ResponseModel;
}
