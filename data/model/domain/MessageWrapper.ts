import { DomainModel } from "./DomainModel";

export default interface MessageWrapper extends DomainModel {
    readonly message: string;
}
