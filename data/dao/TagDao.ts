import { PoolConnection, UpsertResult } from "mariadb";
import { TagCreation, TagEntity } from "../model/db/Tag";

export default class TagDao {

    private readonly conn: PoolConnection;
    
    constructor(connection: PoolConnection) {
        this.conn = connection;
    }

    async findTagsByNames(names: string[], limit: number): Promise<TagEntity[]> {
        return await this.conn.query("SELECT * FROM tags t WHERE name IN (?) LIMIT ?", [names, limit]);
    }

    async insertTags(createion: TagCreation[]): Promise<UpsertResult> {
        const values: string[][] = createion.map((c) => {
            return [c.name];
        });

        const result = await this.conn.batch({
            sql: `INSERT IGNORE INTO tags (name) VALUES (?)`,
            insertIdAsNumber: true
        }, values);

        return result as unknown as UpsertResult;
    }

}