import { PoolConnection, UpsertResult } from "mariadb";
import BadRequestException from "../../exception/BadRequestException";
import { MilestoneEntity, MilestoneCreation } from "../model/db/Milestone";

export default class MilestoneDao {

    private readonly conn: PoolConnection;

    constructor(connection: PoolConnection) {
        this.conn = connection;
    }

    async getAllForTask(taskId: number): Promise<MilestoneEntity[]> {
        const raw = await this.conn.query("SELECT * FROM milestones WHERE taskId=?", [taskId]);

        return raw as MilestoneEntity[];
    }

    async inserForTask(creation: MilestoneCreation): Promise<UpsertResult> {
        const taskExist = await this.conn.query(`SELECT IF(
                EXISTS (SELECT * FROM tasks WHERE id=?), 'yes', 'no'
            ) as RESULT`, [creation.taskId]);

        if (taskExist[0].RESULT === 'no') {
            throw new BadRequestException(
                `task_with_id_${creation.taskId}_not_exists`,
                "Task not exists"
            );
        }

        const raw = await this.conn.query({
            sql: "INSERT INTO milestones (taskId, name) VALUES (?, ?)",
            insertIdAsNumber: true
        }, [creation.taskId, creation.name]);

        return raw;
    }
}