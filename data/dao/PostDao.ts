import { PoolConnection, UpsertResult } from "mariadb";
import { PostCreation, PostEntity } from "../model/db/Post";
import { TagEntityPost } from "../model/db/Tag";

export default class PostDao {

    private readonly conn: PoolConnection

    constructor(connection: PoolConnection) {
        this.conn = connection;
    }

    async findAll(): Promise<PostEntity[]> {
        const raw = await this.conn.query({
            sql: `SELECT * FROM posts p 
                LEFT JOIN post_tag tp ON p.id=tp.postId
                LEFT JOIN tags t ON t.id=tp.tagId`,
            nestTables: true
        });

        const result: PostEntity[] = [];

        for (let row of raw) {
            let lastPost: PostEntity | undefined;
            if (result.length > 0) {
                lastPost = result[result.length - 1];
            }
            if (lastPost?.id === row.p.id) {
                if (row.t.id) {
                    lastPost!.tags!.push(row.t);
                }
                continue;
            }
            const tags: TagEntityPost[] = [];
            if (row.t.id) {
                tags.push(row.t);
            }
            result.push({
                id: row.p.id,
                name: row.p.name,
                tags: tags
            });
        }

        return result;
    }

    async insertPost(creation: PostCreation): Promise<UpsertResult> {
        return await this.conn.query({
            sql: `INSERT INTO posts (name) VALUES (?)`,
            insertIdAsNumber: true
        }, [creation.name]);
    }

}