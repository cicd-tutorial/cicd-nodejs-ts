import { PoolConnection, UpsertResult } from "mariadb";
import { MilestoneEntity } from "../model/db/Milestone";
import { TaskCreation, TaskEntity } from "../model/db/Task";

export default class TaskDao {

    private readonly conn: PoolConnection;

    constructor(connection: PoolConnection) {
        this.conn = connection;
    }

    async getAllWithMilestones(): Promise<TaskEntity[]> {
        const raw = await this.conn.query({
            sql: "SELECT * FROM tasks t LEFT JOIN milestones m ON t.id=m.taskId",
            nestTables: true
        });

        const result: TaskEntity[] = [];

        for (let row of raw) {
            let lastTask: TaskEntity | undefined;
            if (result.length > 0) {
                lastTask = result[result.length - 1];
            }
            if (lastTask?.id === row.t.id) {
                if (row.m.id) {
                    lastTask!.milestones!.push(row.m);
                }
                continue;
            }
            const milestones: MilestoneEntity[] = []
            if (row.m.id) {
                milestones.push(row.m);
            }
            result.push({
                id: row.t.id,
                name: row.t.name,
                milestones: milestones
            });
        }
        return result;
    }

    async insert(creation: TaskCreation): Promise<UpsertResult> {
        const raw = await this.conn.query({
            sql: "INSERT INTO tasks (name) VALUES (?)",
            insertIdAsNumber: true
        }, [creation.name]);

        return raw;
    }

}