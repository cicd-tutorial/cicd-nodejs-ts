import { PoolConnection, UpsertResult } from "mariadb";
import { PostTagCreation } from "../model/db/PostTag";

export default class PostTagDao {

    private readonly conn: PoolConnection;

    constructor(connection: PoolConnection) {
        this.conn = connection;
    }

    async insertPostTags(createion: PostTagCreation[]): Promise<UpsertResult> {

        const values: number[][] = createion.map((c) => {
            return [c.postId, c.tagId];
        });

        const result = await this.conn.batch({
            sql: `INSERT INTO post_tag (postId, tagId) VALUES (?, ?)`,
            insertIdAsNumber: true
        }, values);

        return result as unknown as UpsertResult;
    }
}