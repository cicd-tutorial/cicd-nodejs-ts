import { PoolConnection, UpsertResult } from "mariadb";
import { safeInsertHandle, safeQueryHandle } from "../../../util/DaoUtils";
import MilestoneDao from "../../dao/MilestoneDao";
import { MilestoneCreation, MilestoneEntity } from "../../model/db/Milestone";
import MilstoneRepository from "../MilestoneRepository";

export default class MilstoneRepositoryImpl implements MilstoneRepository {

    async getAllMilestonesForTask(taskId: number): Promise<MilestoneEntity[]> {
        return await safeQueryHandle(async (conn: PoolConnection): Promise<MilestoneEntity[]> => {
            const dao = new MilestoneDao(conn);
            return await dao.getAllForTask(taskId);
        });

    }

    async createMilestone(creation: MilestoneCreation): Promise<UpsertResult> {
        return await safeInsertHandle(async (conn: PoolConnection): Promise<UpsertResult> => {
            const dao = new MilestoneDao(conn);
            return await dao.inserForTask(creation);
        })
    }

}