import { PoolConnection, UpsertResult } from "mariadb";
import { safeInsertHandle, safeQueryHandle } from "../../../util/DaoUtils";
import PostDao from "../../dao/PostDao";
import PostTagDao from "../../dao/PostTagDao";
import TagDao from "../../dao/TagDao";
import { PostCreation, PostEntity } from "../../model/db/Post";
import { PostTagCreation } from "../../model/db/PostTag";
import { TagCreation } from "../../model/db/Tag";
import PostRepository from "../PostRepository";

export default class PostRepositoryImpl implements PostRepository {

    async getAllPosts(): Promise<PostEntity[]> {
        return await safeQueryHandle(async (conn: PoolConnection): Promise<PostEntity[]> => {
            const dao = new PostDao(conn);
            return await dao.findAll();
        });
    }

    async insertPost(creation: PostCreation, tagsCreation: TagCreation[]): Promise<UpsertResult> {
        return await safeInsertHandle(async (conn: PoolConnection): Promise<UpsertResult> => {
            const postDao = new PostDao(conn);

            const postResult = await postDao.insertPost(creation);

            if (tagsCreation.length !== 0) {
                const tagDao = new TagDao(conn);
                const postTagDao = new PostTagDao(conn);

                const tagsResult = await tagDao.insertTags(tagsCreation);
                const postTagCreation: PostTagCreation[] = [];

                if (tagsResult.affectedRows !== tagsCreation.length) {
                    const tagNames = tagsCreation.map((c) => {
                        return c.name;
                    });
                    const tags = await tagDao.findTagsByNames(tagNames, tagNames.length);
                    tags.forEach((t) => {
                        postTagCreation.push({
                            postId: postResult.insertId as number,
                            tagId: t.id
                        });
                    });
                } else {
                    let tagId: number = tagsResult.insertId as number;
                    for (let i = 0; i < tagsCreation.length; i++) {
                        postTagCreation.push({
                            postId: postResult.insertId as number,
                            tagId: tagId
                        });
                        tagId++;
                    }
                }

                await postTagDao.insertPostTags(postTagCreation);
            }

            return postResult;
        });

    }

}