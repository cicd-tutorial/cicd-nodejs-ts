import { getRedis } from "../../../util/manager/redisManager";
import ValueRepository from "../ValueRepository";

export default class ValueRepositoryImpl implements ValueRepository {

    async getValue(): Promise<number | null> {
        const redis = await getRedis();

        const valueStr = await redis.get('value');

        if(!valueStr) {
            return null;
        }

        const value = parseInt(valueStr);

        if (isNaN(value)) {
            throw new Error("redis value is not number");
        }
        
        return value;
    }

    async setValue(newValue: number): Promise<boolean> {
        const redis = await getRedis();
  
        const result = await redis.set("value", newValue)
        if(result === "OK") {
            return true;
        }

        return false;
    }

}