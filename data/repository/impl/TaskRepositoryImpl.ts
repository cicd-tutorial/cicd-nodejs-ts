import { PoolConnection, UpsertResult } from "mariadb";
import { safeInsertHandle, safeQueryHandle } from "../../../util/DaoUtils";
import TaskDao from "../../dao/TaskDao";
import { TaskCreation, TaskEntity } from "../../model/db/Task";
import TaskRepostiory from "../TaskRepository";

export default class TaskRepositoryImpl implements TaskRepostiory {

    async getAllTasks(): Promise<TaskEntity[]> {
        return await safeQueryHandle(async (conn: PoolConnection): Promise<TaskEntity[]> => {
            const dao = new TaskDao(conn);
            return await dao.getAllWithMilestones();
        });
    }

    async createTask(creation: TaskCreation): Promise<UpsertResult> {
        return await safeInsertHandle(async (conn: PoolConnection): Promise<UpsertResult> => {
            const dao = new TaskDao(conn);
            return await dao.insert(creation);
        });
    }

}