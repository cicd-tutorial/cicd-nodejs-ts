export default interface ValueRepository {
    getValue(): Promise<number | null>

    setValue(newValue: number): Promise<boolean>
}