import { UpsertResult } from "mariadb";
import { PostCreation, PostEntity } from "../model/db/Post";
import { TagCreation } from "../model/db/Tag";

export default interface PostRepository {

    getAllPosts(): Promise<PostEntity[]>

    insertPost(creation: PostCreation, tagsCreation: TagCreation[]): Promise<UpsertResult>

}