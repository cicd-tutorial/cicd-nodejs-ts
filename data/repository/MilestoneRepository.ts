import { UpsertResult } from "mariadb";
import { MilestoneCreation, MilestoneEntity } from "../model/db/Milestone";

export default interface MilestoneRepository {

    getAllMilestonesForTask(taskId: number): Promise<MilestoneEntity[]>

    createMilestone(creation: MilestoneCreation): Promise<UpsertResult>

}