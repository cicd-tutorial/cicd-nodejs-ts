import { UpsertResult } from "mariadb"
import { TaskCreation, TaskEntity } from "../model/db/Task"

export default interface TaskRepostiory {
    
    getAllTasks(): Promise<TaskEntity[]>

    createTask(creation: TaskCreation): Promise<UpsertResult>
}