import { MilestoneEntity } from "../model/db/Milestone";
import MilestoneResponse from "../model/response/MilestoneResponse";

export default class MilestoneModifiers {
    static entityToResponse(entity: MilestoneEntity): MilestoneResponse {
        return {
            id: entity.id,
            taskId: entity.taskId,
            name: entity.name
        };
    }

    static listEntityToResponse(entities: MilestoneEntity[]): MilestoneResponse[] {
        return entities.map((o) => {
            return this.entityToResponse(o);
        });
    }
}