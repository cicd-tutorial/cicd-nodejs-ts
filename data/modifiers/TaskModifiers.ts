import { TaskEntity } from "../model/db/Task";
import TaskResponse from "../model/response/TaskResponse";

export default class TasksModifiers {

    static entityToResponse(entity: TaskEntity): TaskResponse {
        return {
            id: entity.id,
            name: entity.name,
            milestones: entity.milestones?.map((m) => {
                return {
                    id: m.id,
                    name: m.name
                }
            })
        };
    }

    static listEntityToResponse(entities: TaskEntity[]): TaskResponse[] {
        return entities.map((o) => {
            return this.entityToResponse(o);
        });
    }

}