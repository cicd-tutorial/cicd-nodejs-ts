import MessageWrapper from "../model/domain/MessageWrapper";
import MessageResponse from "../model/response/MessageResponse";

export default class MessageModifier {

    static domainToResponse = (response: MessageWrapper): MessageResponse => {
        return {
            message: response.message
        };
    }

}