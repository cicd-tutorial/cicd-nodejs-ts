import { PostEntity } from "../model/db/Post";
import PostResponse from "../model/response/PostResponse";

export default class PostModifers {

    static entityToResponse(entity: PostEntity): PostResponse {
        return {
            id: entity.id,
            name: entity.name,
            tags: entity.tags?.map((t) => {
                return {
                    id: t.id,
                    name: t.name
                }
            })
        }
    }

    static listEntityToResponse(entities: PostEntity[]): PostResponse[] {
        return entities.map((e) => {
            return this.entityToResponse(e);
        });
    }

}