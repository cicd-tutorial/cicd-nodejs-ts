#!/bin/sh
full_path=$(realpath $0)
 
dir_path=$(dirname $full_path)

docker-compose -p cicd_dev --env-file "$dir_path/../../.env.dev" -f "$dir_path/../docker-compose-dev.yml" up