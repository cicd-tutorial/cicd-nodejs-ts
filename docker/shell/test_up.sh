#!/bin/sh
full_path=$(realpath $0)
 
dir_path=$(dirname $full_path)

docker-compose -p cicd_test --env-file "$dir_path/../../.env.test" -f "$dir_path/../docker-compose-test.yml" up &
while [ $( docker ps -a | grep cicd_web_app_testing | wc -l ) -eq 0 ] || 
    [ $(docker inspect cicd_web_app_testing | jq -r '.[0]["State"]["Running"]') == "false" ] ; do
    sleep 0.1
done
exit_code=$(docker wait cicd_web_app_testing)
test -e "$dir_path/../../coverage-docker" && rm -r ./coverage-docker
docker cp cicd_web_app_testing:/app/coverage "$dir_path/../../coverage-docker"

exit $exit_code