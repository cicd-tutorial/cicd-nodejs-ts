#!/bin/sh
full_path=$(realpath $0)
 
dir_path=$(dirname $full_path)

{
    docker container stop cicd_web_app_dev 2> /dev/null 1> /dev/null &&
    echo "Container cicd_web_app_dev has stoped" 
} || {
    echo "Probably container cicd_web_app_dev not running"
}

{
    docker container rm cicd_web_app_dev 2> /dev/null 1> /dev/null &&
    echo "Container cicd_web_app_dev removed"
} || {
    echo "Probably container cicd_web_app_dev not exist"
}

{
    docker rmi cicd_web_app_dev:1.0  2> /dev/null 1> /dev/null &&
    echo "Image cicd_web_app_dev:1.0 removed"
} || {
    echo "Probably image cicd_web_app_dev:1.05 not exist"
}

docker-compose -p cicd_dev --env-file "$dir_path/../../.env.dev" -f "$dir_path/../docker-compose-dev.yml" build