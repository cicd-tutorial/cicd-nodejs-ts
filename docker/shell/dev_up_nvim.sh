#!/bin/sh
full_path=$(realpath $0)
 
dir_path=$(dirname $full_path)

docker-compose -p cicd_dev --env-file "$dir_path/../../.env.dev" -f "$dir_path/../docker-compose-dev.yml" up &

tries=0
while ! docker exec cicd_web_app_dev echo test > /dev/null; do
	if [ $tries -gt 30 ]; then
		echo "Can not run dev"
		docker-compose -p cicd_dev --env-file "$dir_path/../../.env.dev" -f "$dir_path/../docker-compose-dev.yml" down
		exit 1
	fi
	echo "try N $tries not succeeded"
	tries=`expr $tries + 1`
	sleep 0.1
done

sleep 10
