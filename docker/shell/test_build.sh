#!/bin/sh
full_path=$(realpath $0)
 
dir_path=$(dirname $full_path)

{
    docker container stop cicd_web_app_testing 2> /dev/null 1> /dev/null &&
    echo "Container cicd_web_app_testing has stoped"
} || {
    echo "Probably container cicd_web_app_testing not running"
}

{
    docker container rm cicd_web_app_testing 2> /dev/null 1> /dev/null &&
    echo "Container cicd_web_app_testing removed"
} || {
    echo "Probably container cicd_web_app_testing not exist"
}

{
    docker rmi cicd_web_app_testing:1.0 2> /dev/null 1>/dev/null &&
    echo "Image cicd_web_app_testing:1.0 removed"
} || {
    echo "Probably image cicd_web_app_testing:1.0 not exist"
}

docker-compose -p cicd_test --env-file "$dir_path/../../.env.test" -f "$dir_path/../docker-compose-test.yml" build