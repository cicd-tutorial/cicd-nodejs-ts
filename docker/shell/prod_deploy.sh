#!/bin/sh
full_path=$(realpath $0)
 
dir_path=$(dirname $full_path)

is_nginx_running=$(docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
 exec cicd_nginx test "" = "" 2> /dev/null && echo "true" || echo "false")
if [[ $is_nginx_running == "false" ]]; then
    echo "Nginx is not running, running all services..."
    docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
     build --no-cache
    docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
     up --force-recreate -d
else
    echo "Nginx is running"
    is_blue=$(docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
     exec cicd_nginx test -e /etc/nginx/sites-enabled/blue && echo "true" || echo "false")
    if [[ $is_blue == "true" ]]; then
        echo "Blue is active, switching to green..."
        docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
         build --no-cache cicd_web_green
        docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
         up --no-deps --force-recreate -d cicd_web_green

        tries=1
        while [ $(docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
         exec cicd_web_green netstat -tulpn | grep :::5001 | wc -l) -ne 1 ]; do
            if [ $tries -gt 30 ]; then
              echo "Can not deploy green"
              exit 1
            fi
            echo "try N $tries not succeeded"
            tries=`expr $tries + 1`
            sleep 0.1
        done
        
    else
        echo "Green is active, switching to blue..."
        docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
         build --no-cache cicd_web_blue
        docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
         up --no-deps --force-recreate -d cicd_web_blue
        
        tries=1
        while [ $(docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
         exec cicd_web_blue netstat -tulpn | grep :::5000 | wc -l) -ne 1 ]; do
            if [ $tries -gt 30 ]; then
              echo "Can not deploy green"
              exit 1
            fi
            echo "try N $tries not succeeded"
            tries=`expr $tries + 1`
            sleep 0.1
        done
    fi
    docker-compose -p cicd --env-file "$dir_path/../../.env" -f "$dir_path/../docker-compose-prod.yml" \
     exec cicd_nginx /etc/nginx/switchDeploy.sh
fi
{
  docker rmi $(docker images | awk '{print $1 " " $3}' | grep "<none>" | awk '{print $2}') &&
  echo "Unused images deleted"
} || {
  echo "No unused images for delete"
}
