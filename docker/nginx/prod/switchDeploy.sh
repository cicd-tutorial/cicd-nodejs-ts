#!/bin/sh
####################################
# Cfg
blue="/etc/nginx/sites-enabled/blue"
green="/etc/nginx/sites-enabled/green"

# Functions
reloadNg () {
    # systemctl reload nginx
    nginx -s reload -c /etc/nginx/nginx.conf
}

switch2Green() {
    echo "Switching to: green"
    rm -rf $blue
    ln -s /etc/nginx/sites-available/green $green
    reloadNg
    exit 0
}

switch2Blue() {
    echo "Switching to: blue"
    rm -rf $green
    ln -s /etc/nginx/sites-available/blue $blue
    reloadNg
    exit 0
}

findActive() {
    if [ -L $blue ]; then
        echo "ActiveSite:blue"
        switch2Green
    fi

    if [ -L $green ]; then
        echo "ActiveSite:green"
        switch2Blue
    fi

    echo "No active site!"
    switch2Blue
}

findActive