import MessageWrapper from "../data/model/domain/MessageWrapper";
import BadRequestException from "../exception/BadRequestException";
import ValueRepository from "../data/repository/ValueRepository";
import { TaskCreation, TaskEntity } from "../data/model/db/Task";
import { MilestoneCreation, MilestoneEntity } from "../data/model/db/Milestone";
import TaskRepository from "../data/repository/TaskRepository";
import MilestoneRepository from "../data/repository/MilestoneRepository";
import { PostCreation, PostEntity } from "../data/model/db/Post";
import PostRepository from "../data/repository/PostRepository";
import { TagCreation } from "../data/model/db/Tag";

export default class IndexService {

    readonly valueRepository: ValueRepository;
    readonly taskRepository: TaskRepository;
    readonly milestoneRepository: MilestoneRepository;
    readonly postsRepository: PostRepository;

    constructor(
        valueRepository: ValueRepository,
        taskRepository: TaskRepository,
        milestoneRepository: MilestoneRepository,
        postsRepository: PostRepository
    ) {
        this.valueRepository = valueRepository;
        this.taskRepository = taskRepository;
        this.milestoneRepository = milestoneRepository;
        this.postsRepository = postsRepository;
    }

    async getHomeMessage(): Promise<MessageWrapper> {
        return {
            message: `NODE_ENV is ${process.env.NODE_ENV}`
        };
    }

    async getValue(): Promise<MessageWrapper> {
        let currentValue: number = (await this.valueRepository.getValue()) || 0;

        return {
            message: currentValue.toString()
        };
    }

    async resetValue(): Promise<MessageWrapper> {
        const success = await this.valueRepository.setValue(0);

        if (!success) {
            throw new Error("Can not reset value");
        }

        return {
            message: '0'
        };
    }

    async incrementValue(): Promise<MessageWrapper> {
        const currentValue = await this.valueRepository.getValue();

        const nextValue: number = (currentValue || 0) + 1
        const success = await this.valueRepository.setValue(nextValue);

        if (!success) {
            throw new Error("Can not increment value");
        }

        return {
            message: nextValue.toString()
        };
    }

    async simulateBadRequest(): Promise<MessageWrapper> {
        throw new BadRequestException(
            "name_is_less_than_3_characters",
            "Please enter the name that contains more than 2 characters"
        );
    }

    async getAllTasks(): Promise<TaskEntity[]> {
        return await this.taskRepository.getAllTasks();
    }

    async createTask(creation: TaskCreation): Promise<MessageWrapper> {
        await this.taskRepository.createTask(creation);

        return {
            message: "Task created"
        };
    }

    async getAllMilestonesForTask(taskId: number): Promise<MilestoneEntity[]> {
        return await this.milestoneRepository.getAllMilestonesForTask(taskId);
    }

    async createMilestone(creation: MilestoneCreation): Promise<MessageWrapper> {
        await this.milestoneRepository.createMilestone(creation);

        return {
            message: "Milestone created"
        };
    }

    async createPost(creation: PostCreation, tags: TagCreation[]): Promise<MessageWrapper> {
        await this.postsRepository.insertPost(creation, tags);

        return {
            message: "Post created"
        };
    }

    async getPosts(): Promise<PostEntity[]> {
        return await this.postsRepository.getAllPosts();
    }

}