import MilestoneRepository from "../data/repository/MilestoneRepository";
import PostRepository from "../data/repository/PostRepository";
import TaskRepostiory from "../data/repository/TaskRepository";
import ValueRepository from "../data/repository/ValueRepository";

export default interface RepositoryProvider {
    getValueRepository(): ValueRepository 

    getTaskRepository(): TaskRepostiory

    getMilestoneRepository(): MilestoneRepository

    getPostRepository(): PostRepository
}