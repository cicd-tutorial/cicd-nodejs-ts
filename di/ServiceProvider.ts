import IndexService from "../services/IndexService";
import RepositoryProvider from "./RepositoryProvider";

export default interface ServiceProvider {

    readonly repositoryProvider: RepositoryProvider;

    getIndexService(): IndexService
}