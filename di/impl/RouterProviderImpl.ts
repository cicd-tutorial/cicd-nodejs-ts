import express, { Router } from "express";
import IndexController from "../../controller/IndexController";
import RouterProvider from "../RouterProvider";
import ServiceProvider from "../ServiceProvider";

export default class RouterProviderImpl implements RouterProvider {

    static indexRouter: Router | undefined;

    readonly serviceProvider: ServiceProvider

    constructor(serviceProvider: ServiceProvider) {
        this.serviceProvider = serviceProvider;
    }

    getIndexRouter(): Router {
        if(!RouterProviderImpl.indexRouter) {
            const indexController = new IndexController(this.serviceProvider.getIndexService());

            const router = express.Router();
    
            router.get('/', indexController.getHome);
            router.get('/value', indexController.getValue);
            router.get('/resetValue', indexController.getResetValue);
            router.get('/increment', indexController.getIncerement);
            router.get('/badRequest', indexController.getBadRequest);
            router.get('/tasks', indexController.getAllTasks);
            router.get('/tasks/create', indexController.getCreateTask);
            router.get('/tasks/:taskId/milestones', indexController.getTaskMilestones);
            router.get('/tasks/:taskId/milestones/create', indexController.getCreateMilestone);
            router.get('/posts', indexController.getPosts);
            router.get('/posts/create', indexController.getCreatePost);

            RouterProviderImpl.indexRouter = router;
        }


        return RouterProviderImpl.indexRouter;
    }

}