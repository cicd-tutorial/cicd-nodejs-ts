import TaskRepositoryImpl from "../../data/repository/impl/TaskRepositoryImpl";
import ValueRepositoryImpl from "../../data/repository/impl/ValueRepositoryImpl";
import MilestoneRepositoryImpl from "../../data/repository/impl/MilestoneRepositoryImpl";
import MilestoneRepository from "../../data/repository/MilestoneRepository";
import TaskRepository from "../../data/repository/TaskRepository";
import ValueRepository from "../../data/repository/ValueRepository";
import RepositoryProvider from "../RepositoryProvider";
import PostRepository from "../../data/repository/PostRepository";
import PostRepositoryImpl from "../../data/repository/impl/PostRepositoryImpl";

export default class RepositoryProviderImpl implements RepositoryProvider {

    static valueRepository: ValueRepository | undefined;
    static taskRespository: TaskRepository | undefined;
    static milestoneRepository: MilestoneRepository | undefined;
    static postsRepository: PostRepository | undefined;

    getValueRepository(): ValueRepository {
        if(!RepositoryProviderImpl.valueRepository) {
            RepositoryProviderImpl.valueRepository = new ValueRepositoryImpl();
        }
        return RepositoryProviderImpl.valueRepository;
    }
    
    getTaskRepository(): TaskRepository {
        if(!RepositoryProviderImpl.taskRespository) {
            RepositoryProviderImpl.taskRespository = new TaskRepositoryImpl();
        }
        return RepositoryProviderImpl.taskRespository;
    }

    getMilestoneRepository(): MilestoneRepository {
        if(!RepositoryProviderImpl.milestoneRepository) {
            RepositoryProviderImpl.milestoneRepository = new MilestoneRepositoryImpl();
        }
        return RepositoryProviderImpl.milestoneRepository;
    }

    getPostRepository(): PostRepository {
        if(!RepositoryProviderImpl.postsRepository) {
            RepositoryProviderImpl.postsRepository = new PostRepositoryImpl();
        }
        return RepositoryProviderImpl.postsRepository;
    }

}