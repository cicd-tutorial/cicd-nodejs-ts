import IndexService from "../../services/IndexService";
import RepositoryProvider from "../RepositoryProvider";
import ServiceProvider from "../ServiceProvider";

export default class ServiceProviderImpl implements ServiceProvider {

    static indexService: IndexService | undefined;

    readonly repositoryProvider: RepositoryProvider

    constructor(repositoryProvider: RepositoryProvider) {
        this.repositoryProvider = repositoryProvider;
    }

    getIndexService(): IndexService {
        if(!ServiceProviderImpl.indexService) {
            ServiceProviderImpl.indexService = new IndexService(
                this.repositoryProvider.getValueRepository(),
                this.repositoryProvider.getTaskRepository(),
                this.repositoryProvider.getMilestoneRepository(),
                this.repositoryProvider.getPostRepository()
            )
        }
        return ServiceProviderImpl.indexService;
    }

}