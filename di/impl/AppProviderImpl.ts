import { Express } from "express";
import AppProvider from "../AppProvider";

import express from "express";
import logger from "morgan";
import errorHandler from "../../controller/error/errorHandler";
import notFoundErrorhandler from "../../controller/error/notFoundErrorHandler";

import RouterProvider from "../RouterProvider";

export default class AppProviderImpl implements AppProvider {

    static app: Express | undefined;

    readonly routerProvider: RouterProvider;

    constructor(routerProvider: RouterProvider) {
        this.routerProvider = routerProvider;
    }

    getApp(): Express {
        if(!AppProviderImpl.app) {
            const app = express();

            // Setup morgan
            switch (process.env.NODE_ENV) {
                case "test":
                case "development":
                    app.use(logger('dev'));
                    break;
                default:
                    app.use(logger('combined'));
                    break;
            }
        
            app.use(express.json());
            app.use(express.urlencoded({ extended: false }));
        
            app.use('/', this.routerProvider.getIndexRouter());
            app.use(errorHandler);
            app.use(notFoundErrorhandler);
            AppProviderImpl.app = app
        }
        return AppProviderImpl.app;
    }

}