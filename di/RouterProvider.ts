import { Router } from "express";
import ServiceProvider from "./ServiceProvider";

export default interface RouterProvider {

    readonly serviceProvider: ServiceProvider;

    getIndexRouter(): Router
}