import { Express } from "express"
import RouterProvider from "./RouterProvider";

export default interface AppProvider {

    readonly routerProvider: RouterProvider;

    getApp(): Express

}