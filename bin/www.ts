#!/usr/bin/env node

/**
 * Module dependencies.
 */
import debugFactory from "debug"
import http from "http";

import { mariadbMigrate } from "./migrate";
import AppProviderImpl from "../di/impl/AppProviderImpl";
import AppProvider from "../di/AppProvider";
import ServiceProviderImpl from "../di/impl/ServiceProviderImpl";
import RepositoryProviderImpl from "../di/impl/RepositoryProviderImpl";
import RouterProviderImpl from "../di/impl/RouterProviderImpl";

const debug = debugFactory('cicd:server');

/**
 * Get port from env or 3000
 */
const getPortFromEnv = (): string => {
  const nodeEnv = process.env.NODE_ENV;
  if (nodeEnv === "test" || nodeEnv === "development") {
    return process.env.PORT!;
  }
  if (process.env.NODE_ENV === "blue") {
    return process.env.PORT_BLUE!;
  }
  if (process.env.NODE_ENV === "green") {
    return process.env.PORT_GREEN!;
  }
  return '3000';
}

/**
 * Normalize a port into a number, string, or false.
 */
const normalizePort = (val: string): number | string | false => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error: any): void => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
const onListening = (): void => {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr?.port;
  debug('Listening on ' + bind);
}

const startServer = (): void => {
  /**
   * Listen on provided port, on all network interfaces.
   */
  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
}

/**
 * Get port from environment and store in Express.
 */
const appProvider: AppProvider = new AppProviderImpl(
  new RouterProviderImpl(
    new ServiceProviderImpl(
      new RepositoryProviderImpl()
    )
  )
)
const app = appProvider.getApp();

const port = normalizePort(getPortFromEnv());
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

mariadbMigrate().then(() => {
  startServer();
}).catch((e) => {
  console.error("migrations failed");
  console.error(e);
  process.exit(1);
});