import { PoolConnection } from "mariadb";

import { safeInsertHandle } from "../util/DaoUtils";

interface MigrationInfo {
    readonly migration_name: string;
}

class MigrationManager {
    private readonly getMigrationInfo = async (conn: PoolConnection, migration_name: string): Promise<MigrationInfo | null> => {
        const raw = await conn.query("SELECT * FROM migration_info WHERE migration_name LIKE ?", [migration_name]);
        if (raw.length === 0) {
            return null;
        }
        return raw[0] as MigrationInfo;
    }

    private readonly createMigrationInfo = async (conn: PoolConnection, migration_name: string): Promise<void> => {
        await conn.query("INSERT INTO migration_info (migration_name) VALUES (?)", [migration_name]);
    }

    private readonly migrate = async (sql: string, migrationName: string): Promise<void> => {
        return await safeInsertHandle(async (conn: PoolConnection): Promise<void> => {
            const migInfo = await this.getMigrationInfo(conn, migrationName);
            if (!migInfo) {
                await conn.query(sql);
                await this.createMigrationInfo(conn, migrationName);
                console.log(`${migrationName} migrated`);
                return;
            }
            console.log(`${migrationName} up to date`);
        });
    }

    private readonly createMigrationsInfoTable = async (): Promise<void> => {
        return await safeInsertHandle(async (conn: PoolConnection): Promise<void> => {
            await conn.query(`CREATE TABLE IF NOT EXISTS migration_info (
                migration_name varchar(255) NOT NULL,
                UNIQUE KEY migration_name (migration_name)
            )`);
            console.log("migration_info table available");
        });
    }

    private readonly createTasksTable = async (): Promise<void> => {
        return await this.migrate(`CREATE TABLE tasks (
            id int(11) NOT NULL AUTO_INCREMENT,
            name varchar(255) NOT NULL,
            PRIMARY KEY (id)
        )`, "createTasksTable");
    }

    private readonly createMilestoneTable = async (): Promise<void> => {
        return await this.migrate(`CREATE TABLE milestones (
            id int(11) NOT NULL AUTO_INCREMENT,
            taskId int(11) NOT NULL,
            name varchar(255) NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT milestones_fk_1 FOREIGN KEY (taskId) REFERENCES tasks (id)
        )`, "createMilestoneTable");
    }

    private readonly createPostsTable = async (): Promise<void> => {
        return await this.migrate(`CREATE TABLE posts (
            id int(11) NOT NULL AUTO_INCREMENT,
            name varchar(255) NOT NULL,
            PRIMARY KEY (id)
        )`, "createPostsTable");
    }

    private readonly createTagsTable = async (): Promise<void> => {
        return await this.migrate(`CREATE TABLE tags (
            id int(11) NOT NULL AUTO_INCREMENT,
            name varchar(255) NOT NULL,
            PRIMARY KEY (id)
        )`, "createTagsTable");
    }

    private readonly createPost_TagTable = async (): Promise<void> => {
        return await this.migrate(`CREATE TABLE post_tag (
            postId int(11) NOT NULL,
            tagId int(11) NOT NULL,
            CONSTRAINT post_tag_fk_1 FOREIGN KEY (postId) REFERENCES posts (id),
            CONSTRAINT post_tag_fk_2 FOREIGN KEY (tagId) REFERENCES tags (id)
        )`, "createPost_TagTable");
    }

    private readonly makeTagsNameUnique = async (): Promise<void> => {
        return await this.migrate(`ALTER TABLE tags
        ADD CONSTRAINT tags_uk_1 UNIQUE KEY (name)`,
            "makeTagsNameUnique");
    }

    private migrations: (() => Promise<void>)[] = [
        this.createMigrationsInfoTable,
        this.createTasksTable,
        this.createMilestoneTable,
        this.createPostsTable,
        this.createTagsTable,
        this.createPost_TagTable,
        this.makeTagsNameUnique
    ];

    waitForMigrations = async (): Promise<void> => {
        console.log("==================== starting migrations ====================");
        for (let m of this.migrations) {
            await m();
        }
        console.log("==================== migrations done ====================");
    }
}

const mariadbMigrate = async () => {
    const migrationManager: MigrationManager = new MigrationManager();
    await migrationManager.waitForMigrations();
}

export { mariadbMigrate };