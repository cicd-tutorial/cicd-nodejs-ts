export default class BadRequestException extends Error {
    readonly reason: string;
    readonly msg: string;

    constructor(reason: string, msg: string) {
        super("Bad Request");
        this.reason = reason;
        this.msg = msg;
    }
}