export default class RedisConnectionTimeoutException extends Error { 

    constructor() {
        super("Can not connect to redis");
    }
}