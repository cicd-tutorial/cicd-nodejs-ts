module.exports = {
    apps: [
        {
            name: "cicd",
            script: "build/bin/www.js",
            exec_mode : "cluster"
        },
        {
            name: "cicd_local",
            script: "build/bin/www.js",
            watch: "build",
            exec_mode : "fork",
            node_args: "--require dotenv/config",
            env: {
                DOTENV_CONFIG_PATH: "./.env.local",
                DEBUG: "*:*,-axm:*"
            }
        }
    ]
}