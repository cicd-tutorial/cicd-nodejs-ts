local _, dap = pcall(require, "dap")
if dap then
	local dap_configs = {
		{
			command = "yarn start:pm2:local",
			name = "pm2 local nvim",
			request = "launch",
			type = "pwa-node",
		},
		{
			command = "yarn start:local",
			name = "nodemon local nvim",
			request = "launch",
			type = "pwa-node",
		},
		{
			type = "pwa-node",
			request = "attach",
			name = "Compose dev nvim",
			address = "0.0.0.0",
			port = 9229,
			localRoot = vim.fn.getcwd(),
			remoteRoot = "/app",
			restart = true,
			protocol = "inspector",
			preLaunchTask = "launch_dev_container nvim",
			postDebugTask = "stop_dev_container",
		},
		{
			command = "yarn test:local",
			name = "jest local nvim",
			request = "launch",
			type = "pwa-node",
		},
	}
	for _, c in pairs(dap_configs) do
		table.insert(dap.configurations["typescript"], c)
	end
end

local _, overseer = pcall(require, "overseer")
if overseer then
	overseer.register_template({
		name = "launch_dev_container nvim",
		builder = function()
			return {
				-- cmd is the only required field
				cmd = { "./docker/shell/dev_up_nvim.sh" },

				-- additional arguments for the cmd
				-- args = { "&" },

				-- the name of the task (defaults to the cmd of the task)
				-- name = "launch-dev-container nvim",

				-- set the working directory for the task
				cwd = vim.fn.getcwd(),
				-- additional environment variables
				-- env = {
					-- VAR = "FOO",
				-- },

				-- the list of components or component aliases to add to the task
				-- components = { "my_custom_component", "default" },
				-- arbitrary table of data for your own personal use
				metadata = {
					-- foo = "bar",
				},
			}
		end,
	})
	overseer.register_template({
		name = "stop_dev_container",
		builder = function()
			return {
				-- cmd is the only required field
				cmd = { "docker-compose" },

				-- additional arguments for the cmd
				args = { "-p", "cicd_dev", "stop" },

				-- the name of the task (defaults to the cmd of the task)
				name = "Stop dev container",

				-- set the working directory for the task
				cwd = vim.fn.getcwd(),
				-- additional environment variables
				-- env = {
					-- VAR = "FOO",
				-- },

				-- the list of components or component aliases to add to the task
				-- components = { "my_custom_component", "default" },
				-- arbitrary table of data for your own personal use
				metadata = {
					-- foo = "bar",
				},
			}
		end,
	})
end
