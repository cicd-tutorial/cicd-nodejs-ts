import MilestoneRepository from "../../../data/repository/MilestoneRepository";
import PostRepository from "../../../data/repository/PostRepository";
import TaskRepository from "../../../data/repository/TaskRepository";
import ValueRepository from "../../../data/repository/ValueRepository";
import RepositoryProvider from "../../../di/RepositoryProvider";
import FakeMilestoneRepository from "../repository/FakeMilestoneRepository";
import FakePostRepository from "../repository/FakePostRepository";
import FakeTaskRepository from "../repository/FakeTaskRepostiory";
import FakeValueRepository from "../repository/FakeValueRepository";

export default class FakeRepositoryProvider implements RepositoryProvider {

    static valueRepository: ValueRepository | undefined;
    static taskRepository: TaskRepository | undefined;
    static milestoneRepository: MilestoneRepository | undefined;
    static postRepository: PostRepository | undefined;

    getValueRepository(): ValueRepository {
        if(!FakeRepositoryProvider.valueRepository) {
            FakeRepositoryProvider.valueRepository = new FakeValueRepository();
        }
        return FakeRepositoryProvider.valueRepository;
    }
    getTaskRepository(): TaskRepository {
        if(!FakeRepositoryProvider.taskRepository) {
            FakeRepositoryProvider.taskRepository = new FakeTaskRepository();
        }
        return FakeRepositoryProvider.taskRepository;
    }
    getMilestoneRepository(): MilestoneRepository {
        if(!FakeRepositoryProvider.milestoneRepository) {
            FakeRepositoryProvider.milestoneRepository = new FakeMilestoneRepository();
        }
        return FakeRepositoryProvider.milestoneRepository;
    }

    getPostRepository(): PostRepository {
        if(!FakeRepositoryProvider.postRepository) {
            FakeRepositoryProvider.postRepository = new FakePostRepository();
        }
        return FakeRepositoryProvider.postRepository;
    }

}