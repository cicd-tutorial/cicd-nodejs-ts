import { UpsertResult } from "mariadb";
import { MilestoneEntity, MilestoneCreation } from "../../../data/model/db/Milestone";
import MilestoneRepository from "../../../data/repository/MilestoneRepository";
import BadRequestException from "../../../exception/BadRequestException";
import { fakeTasks } from "../storage/FakeStorage";

export default class FakeMilestoneRepository implements MilestoneRepository {

    async getAllMilestonesForTask(taskId: number): Promise<MilestoneEntity[]> {
        const task = fakeTasks.find((t) => {
            return t.id === taskId;
        });
        if(task) {
            return task.milestones!;
        }
        return [];
    }
    async createMilestone(creation: MilestoneCreation): Promise<UpsertResult> {
        const task = fakeTasks.find((t) => {
            return t.id === creation.taskId
        });
        if (!task) {
            throw new BadRequestException(
                `task_with_id_${creation.taskId}_not_exists`,
                "Task not exists"
            );
        }
        const lastId = task.milestones![task.milestones!.length - 1]?.id || -1
        task.milestones!.push({
            id: lastId + 1,
            taskId: creation.taskId,
            name: creation.name,
        });
        return {
            affectedRows: 1,
            insertId: lastId + 1,
            warningStatus: 0
        };
    }

}