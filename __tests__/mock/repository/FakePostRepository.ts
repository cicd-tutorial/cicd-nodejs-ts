import { UpsertResult } from "mariadb";
import { PostEntity, PostCreation } from "../../../data/model/db/Post";
import { TagCreation } from "../../../data/model/db/Tag";
import PostRepository from "../../../data/repository/PostRepository";

export default class FakePostRepository implements PostRepository {
    getAllPosts(): Promise<PostEntity[]> {
        throw new Error("Method not implemented.");
    }
    insertPost(creation: PostCreation, tagsCreation: TagCreation[]): Promise<UpsertResult> {
        throw new Error("Method not implemented.");
    }

}