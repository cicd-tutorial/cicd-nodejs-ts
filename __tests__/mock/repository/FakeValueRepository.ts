import ValueRepository from "../../../data/repository/ValueRepository";
import { fakeValue, setFakeValue } from "../storage/FakeStorage";

export default class FakeValueRepository implements ValueRepository {
    async getValue(): Promise<number | null> {
        return fakeValue;
    }
    async setValue(newValue: number): Promise<boolean> {
        setFakeValue(newValue);
        return true;
    }

}