import { UpsertResult } from "mariadb";
import { TaskEntity, TaskCreation } from "../../../data/model/db/Task";
import TaskRepostiory from "../../../data/repository/TaskRepository";
import { fakeTasks } from "../storage/FakeStorage";

export default class FakeTaskRepository implements TaskRepostiory {

    async getAllTasks(): Promise<TaskEntity[]> {
        return fakeTasks;
    }
    async createTask(creation: TaskCreation): Promise<UpsertResult> {
        const lastId = fakeTasks[fakeTasks.length - 1]?.id || -1;
        fakeTasks.push({
            id: lastId + 1,
            name: creation.name,
            milestones: []
        });
        return {
            affectedRows: 1,
            insertId: lastId + 1,
            warningStatus: 0
        }
    }

}