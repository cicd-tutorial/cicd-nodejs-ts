import { TaskEntity } from "../../../data/model/db/Task";

export const fakeTasks: TaskEntity[] = [];

export let fakeValue: number | null = null;

export const setFakeValue = (newValue: number | null): void => {
    fakeValue = newValue;
}