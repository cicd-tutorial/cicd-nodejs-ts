import AppProviderImpl from "../../di/impl/AppProviderImpl";
import RouterProviderImpl from "../../di/impl/RouterProviderImpl";
import ServiceProviderImpl from "../../di/impl/ServiceProviderImpl";
import FakeRepositoryProvider from "./di/FakeRepositoryProvider";

export const fakeAppProvider = new AppProviderImpl(
    new RouterProviderImpl(
        new ServiceProviderImpl(
            new FakeRepositoryProvider()
        )
    )
);