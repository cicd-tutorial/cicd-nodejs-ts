import { MilestoneCreation } from "../../data/model/db/Milestone";
import { TaskEntity } from "../../data/model/db/Task";
import MessageWrapper from "../../data/model/domain/MessageWrapper";
import MilestoneModifiers from "../../data/modifiers/MilestoneModifers";
import TasksModifiers from "../../data/modifiers/TaskModifiers";
import BadRequestException from "../../exception/BadRequestException";
import { fakeAppProvider } from "../mock/fakeAppProvider";
import { fakeTasks, fakeValue, setFakeValue } from "../mock/storage/FakeStorage";

describe("IndexService test", () => {



    const indexService = fakeAppProvider
        .routerProvider
        .serviceProvider
        .getIndexService();

    test("test getHomeMessaeg", async () => {
        const result = await indexService.getHomeMessage();

        const validResult: MessageWrapper = {
            message: `NODE_ENV is ${process.env.NODE_ENV}`
        };

        expect(result).toEqual(validResult);
    });

    test("test getValue when value is null", async () => {
        setFakeValue(null);

        const result = await indexService.getValue();

        const validResult: MessageWrapper = {
            message: '0'
        };

        expect(result).toEqual(validResult);
    });

    test("test getValue when value is not null", async () => {
        setFakeValue(5);

        const result = await indexService.getValue();

        const validResult: MessageWrapper = {
            message: '5'
        };

        expect(result).toEqual(validResult);
    });

    test("test resetValue", async () => {
        setFakeValue(10);

        const result = await indexService.resetValue();

        const validResult: MessageWrapper = {
            message: '0'
        };

        expect(fakeValue).toEqual(0);
        expect(result).toEqual(validResult);
    });

    test("test incrementValue when value null", async () => {
        setFakeValue(null);

        const result = await indexService.incrementValue();

        const validResult: MessageWrapper = {
            message: '1'
        };

        expect(fakeValue).toEqual(1);
        expect(result).toEqual(validResult);
    });

    test("test incrementValue when value not null", async () => {
        setFakeValue(6);

        const result = await indexService.incrementValue();

        const validResult: MessageWrapper = {
            message: '7'
        };

        expect(fakeValue).toEqual(7);
        expect(result).toEqual(validResult);
    });

    test("test simulateBadRequest", async () => {

        const validError: BadRequestException = new BadRequestException(
            "name_is_less_than_3_characters",
            "Please enter the name that contains more than 2 characters"
        );

        await expect(indexService.simulateBadRequest()).rejects.toThrowError(validError);
    });

    test("test getAllTasks", async () => {
        const result = await indexService.getAllTasks();

        const validResult = TasksModifiers.listEntityToResponse(fakeTasks);

        expect(result).toEqual(validResult);
    });

    test("test createTask", async () => {
        const result = await indexService.createTask({
            name: "asd"
        });

        const validResult: MessageWrapper = {
            message: "Task created"
        };

        expect(result).toEqual(validResult);
        expect(fakeTasks.find((o) => {
            return o.name === "asd";
        })).not.toBeUndefined();
    });

    test("test getAllMilestonesForTask", async () => {
        const mockTask: TaskEntity = {
            id: 123,
            name: "qweewq",
            milestones: [
                {
                    id: 1234,
                    taskId: 123,
                    name: "asd1"
                },
                {
                    id: 12345,
                    taskId: 123,
                    name: "qwe1"
                }
            ]
        };

        fakeTasks.push(mockTask);

        const result = await indexService.getAllMilestonesForTask(mockTask.id);

        expect(result).toEqual(MilestoneModifiers.listEntityToResponse(mockTask.milestones!));
    });

    test("test createMilestone", async () => {
        const mockTask: TaskEntity = {
            id: 1234,
            name: "qweewq",
            milestones: [
                {
                    id: 12345,
                    taskId: 1234,
                    name: "asd1"
                },
                {
                    id: 123456,
                    taskId: 1234,
                    name: "qwe1"
                }
            ]
        };

        fakeTasks.push(mockTask);

        const creation: MilestoneCreation = {
            taskId: mockTask.id,
            name: "testTestTest"
        }

        const result = await indexService.createMilestone(creation);

        const validResult: MessageWrapper = {
            message: "Milestone created"
        };

        expect(result).toEqual(validResult);

        const created = fakeTasks.find((t) => {
            return t.id === mockTask.id
        })?.milestones!.find((m) => {
            return m.name === creation.name && m.taskId === creation.taskId;
        });
        expect(created).not.toBeUndefined();
    });
});