import supertest from "supertest";

import MessageResponse from "../../data/model/response/MessageResponse";
import ErrorResponse from "../../data/model/response/ErrorResponse";
import { fakeTasks, fakeValue, setFakeValue } from "../mock/storage/FakeStorage";
import { fakeAppProvider } from "../mock/fakeAppProvider";
import { TaskEntity } from "../../data/model/db/Task";
import TasksModifiers from "../../data/modifiers/TaskModifiers";
import MilestoneModifiers from "../../data/modifiers/MilestoneModifers";
import MilestoneResponse from "../../data/model/response/MilestoneResponse";

describe("index router test", () => {

    const app = fakeAppProvider.getApp();

    test("get / test", async () => {
        const result = await supertest(app).get('/');

        const validResponse: MessageResponse = {
            message: `NODE_ENV is ${process.env.NODE_ENV}`
        };

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
    });

    test("get /value when value is empty test", async () => {
        setFakeValue(null);

        const validResponse: MessageResponse = {
            message: "0"
        };

        const result = await supertest(app).get('/value');

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
    });

    test("get /value when value is not empty test", async () => {
        setFakeValue(5);

        const validResponse: MessageResponse = {
            message: "5"
        };

        const result = await supertest(app).get('/value');

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
    });

    test("get /resetValue test", async () => {
        setFakeValue(5);

        const result = await supertest(app).get('/resetValue');

        const validResponse: MessageResponse = {
            message: '0'
        };

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
        expect(fakeValue).toEqual(0);
    });

    test("get /increment when value is not empty test", async () => {
        setFakeValue(5);

        const result = await supertest(app).get('/increment');

        const validResponse: MessageResponse = {
            message: '6'
        };

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
        expect(fakeValue).toEqual(6);
    });

    test("get /increment when value is empty test", async () => {
        setFakeValue(null);

        const result = await supertest(app).get('/increment');

        const validResponse: MessageResponse = {
            message: '1'
        };

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
        expect(fakeValue).toEqual(1);
    });

    test("get /badRequest should respond bad request", async () => {
        const result = await supertest(app).get('/badRequest');

        const validErrorResposne: ErrorResponse = {
            code: 400,
            reason: "name_is_less_than_3_characters",
            message: "Please enter the name that contains more than 2 characters"
        };

        expect(result.statusCode).toEqual(400);
        expect(result.body).toEqual(validErrorResposne);
    });

    test("get /tasks test", async () => {
        fakeTasks.length = 0;

        const mockTaks: TaskEntity[] = [
            {
                id: 1,
                name: "asd",
                milestones: [
                    {
                        id: 1,
                        taskId: 1,
                        name: "123"
                    }, {
                        id: 2,
                        taskId: 1,
                        name: "345"
                    }
                ]
            },
            {
                id: 2,
                name: "qwe",
                milestones: []
            }
        ];

        fakeTasks.push(...mockTaks);

        const validResponse = TasksModifiers.listEntityToResponse(mockTaks);

        const result = await supertest(app).get('/tasks');

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
    });

    test("get /tasks/create", async () => {
        const result = await supertest(app).get('/tasks/create')
            .query({
                name: "asd"
            });

        const validResponse: MessageResponse = {
            message: "Task created"
        };

        expect(result.statusCode).toEqual(201);
        expect(result.body).toEqual(validResponse);

        expect(fakeTasks.find((t) => {
            return t.name === "asd"
        })).not.toBeUndefined();
    });

    test("get /tasks/:taskId/milestones", async () => {
        fakeTasks.length = 0;

        const mockTaks: TaskEntity[] = [
            {
                id: 1,
                name: "asd",
                milestones: [
                    {
                        id: 1,
                        taskId: 1,
                        name: "123"
                    }, {
                        id: 2,
                        taskId: 1,
                        name: "345"
                    }
                ]
            }
        ];

        fakeTasks.push(...mockTaks);

        const validResponse: MilestoneResponse[] = MilestoneModifiers.listEntityToResponse(mockTaks[0].milestones!);

        const result = await supertest(app).get('/tasks/1/milestones');

        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual(validResponse);
    });

    test("get /tasks/:taskId/milestones/create", async () => {
        fakeTasks.length = 0;

        const mockTaks: TaskEntity[] = [
            {
                id: 1,
                name: "asd",
                milestones: []
            }
        ];

        fakeTasks.push(...mockTaks);

        const validResponse: MessageResponse = {
            message: "Milestone created"
        };

        const result = await supertest(app).get('/tasks/1/milestones/create')
            .query({
                name: "asd"
            });

        expect(result.statusCode).toEqual(201);
        expect(result.body).toEqual(validResponse);

        expect(fakeTasks[0].milestones!.find((m) => {
            return m.name === "asd" && m.taskId === 1;
        })).not.toBeUndefined();
    });
});