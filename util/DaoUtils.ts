import { PoolConnection } from "mariadb";

import { contenctionPool } from "./manager/mariadbManager";

const safeInsertHandle = async <T>(action: (connection: PoolConnection) => Promise<T>): Promise<T> => {
    let connection: PoolConnection | undefined
    try {
        connection = await contenctionPool.getConnection();
        await connection.beginTransaction();
        const result = await action(connection);
        await connection.commit();
        connection.release(); // TODO not sure aboit await here
        return result;
    } catch (e: any) {
        await connection?.rollback();
        connection?.release(); // TODO not sure aboit await here
        throw e;
    }
}

const safeQueryHandle = async <T>(action: (connection: PoolConnection) => Promise<T>): Promise<T> => {
    let connection: PoolConnection | undefined
    try {
        connection = await contenctionPool.getConnection();
        const result = await action(connection);
        connection.release(); // TODO not sure aboit await here
        return result;
    } catch (e: any) {
        connection?.release(); // TODO not sure aboit await here
        throw e;
    }
}

export { safeInsertHandle, safeQueryHandle };