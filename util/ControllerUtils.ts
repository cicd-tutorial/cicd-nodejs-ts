import { NextFunction, Request, RequestHandler, Response } from "express";
import { ResponseWithStatus } from "../data/model/domain/ResponseWithStatus";

const safeRequestHandler = (
    handle: (req: Request) => Promise<ResponseWithStatus>
): RequestHandler => {
    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        return await safeRequestHandle(next, async () => {
            const resWithStatus = await handle(req);
            res.status(resWithStatus.status).json(resWithStatus.response);
        });
    };
};

const safeRequestHandle = async (next: NextFunction, handle: () => Promise<void>): Promise<void> => {
    try {
        return await handle();
    } catch(e: any) {
        next(e);
    }
}



export { safeRequestHandler, safeRequestHandle };
