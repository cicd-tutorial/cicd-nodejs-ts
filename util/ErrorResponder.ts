import { Response } from "express";

import ErrorResponse from "../data/model/response/ErrorResponse";
import BadRequestException from "../exception/BadRequestException";

export default class ErrorResponder {

    static respondNotFoundError(res: Response): void {
        const json: ErrorResponse = {
            code: 404,
            reason: "not_found",
            message: "Not Found"
        };
        res.status(404).json(json);
    }

    static respondeInternalError(res: Response): void {
        const json: ErrorResponse = {
            code: 500,
            reason: "internal",
            message: "Internal Server Error"
        };
        res.status(500).json(json);
    }

    static respondBadRequestError(res: Response, ex: BadRequestException): void {
        const json: ErrorResponse = {
            code: 400,
            reason: ex.reason,
            message: ex.msg
        };
        res.status(400).json(json);
    }

}