import { createClient } from "redis";

import { sleep } from "../TSUtils";
import RedisConnectionTimeoutException from "../../exception/RedisConnectionTimeoutException";

const connectionTimeoutMs = 3000;

let isConnecting = false;

// url pattern = redis://<username>:<password>@<host>:<port>
const redisUrl = 'redis://' +
    process.env.REDIS_USERNAME +
    ':' +
    process.env.REDIS_PASSWORD +
    '@' +
    process.env.REDIS_HOST +
    ':' +
    process.env.REDIS_PORT;

const redis = createClient({
    url: redisUrl,
    database: parseInt(process.env.REDIS_DATABASE_INDEX as string)
});

redis.on('error', (err) => {
    isConnecting = false;
    console.error('Redis Client Error', err);
});

redis.on('ready', () => {
    isConnecting = false;
});

const connectIfNotAlready = async (): Promise<void> => {
    if (!isConnecting && !redis.isOpen) {
        isConnecting = true;
        await redis.connect();
    }
};

const waitForConnectionOpen = async (): Promise<void> => {
    await connectIfNotAlready();

    const startedAt = Date.now();
    
    while (!redis.isOpen) {
        await sleep(100);

        const currentTime = Date.now();

        if (currentTime - startedAt > connectionTimeoutMs) {
            throw new RedisConnectionTimeoutException();
        }
    }
};

const getRedis = async () => {
    try {
        await waitForConnectionOpen();
    } catch (e: any) {
        connectIfNotAlready();
        throw e;
    }

    return redis;
};

if (process.env.NODE_ENV !== "test") {
    connectIfNotAlready();
}

export { getRedis };