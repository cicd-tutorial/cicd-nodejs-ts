import mariadb from "mariadb";

const contenctionPool = mariadb.createPool({
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_TCP_PORT!),
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: 5
});

export { contenctionPool };