import { NextFunction, Request, Response, ErrorRequestHandler } from "express";
import BadRequestException from "../../exception/BadRequestException";

import ErrorResponder from "../../util/ErrorResponder";

const errorHandler: ErrorRequestHandler = (err: any, req: Request, res: Response, next: NextFunction): void => {
    if (err instanceof BadRequestException) {
        ErrorResponder.respondBadRequestError(res, err);
        return;
    }

    console.error(err);
    ErrorResponder.respondeInternalError(res);
};

export default errorHandler;