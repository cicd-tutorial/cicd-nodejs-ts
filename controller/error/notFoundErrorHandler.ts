import { Request, Response, NextFunction, RequestHandler } from "express";

import ErrorResponder from "../../util/ErrorResponder";

const notFoundErrorhandler: RequestHandler = (req: Request, res: Response, next: NextFunction): void => {
    ErrorResponder.respondNotFoundError(res);
};

export default notFoundErrorhandler;