import {Request, RequestHandler } from "express";
import { TagCreation } from "../data/model/db/Tag";

import MessageResponse from "../data/model/response/MessageResponse";
import PostResponse from "../data/model/response/PostResponse";
import TaskResponse from "../data/model/response/TaskResponse";
import MessageModifier from "../data/modifiers/MessageModifier";
import MilestoneModifiers from "../data/modifiers/MilestoneModifers";
import PostModifers from "../data/modifiers/PostModifiers";
import TasksModifiers from "../data/modifiers/TaskModifiers";
import BadRequestException from "../exception/BadRequestException";
import IndexService from "../services/IndexService";
import { safeRequestHandler } from "../util/ControllerUtils";

export default class IndexController {

  readonly indexService: IndexService;

  constructor(indexService: IndexService) {
    this.indexService = indexService;
  }

  getHome: RequestHandler = safeRequestHandler(async (req: Request) => {
    const message = await this.indexService.getHomeMessage();

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 200,
      response: response
    };
  });

  getValue: RequestHandler = safeRequestHandler(async (req: Request) => {
    const message = await this.indexService.getValue();

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 200,
      response: response
    };
  });

  getResetValue: RequestHandler = safeRequestHandler(async (req: Request) => {
    const message = await this.indexService.resetValue();

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 200,
      response: response
    };
  });

  getIncerement: RequestHandler = safeRequestHandler(async (req: Request) => {
    const message = await this.indexService.incrementValue();

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 200,
      response: response
    };
  });

  getBadRequest: RequestHandler = safeRequestHandler(async (req: Request) => {
    const message = await this.indexService.simulateBadRequest();

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 200,
      response: response
    };
  });

  getAllTasks: RequestHandler = safeRequestHandler(async (req: Request) => {
    const taskEntities = await this.indexService.getAllTasks();

    const response: TaskResponse[] = TasksModifiers.listEntityToResponse(taskEntities);

    return {
      status: 200,
      response: response
    };
  });

  getCreateTask: RequestHandler = safeRequestHandler(async (req: Request) => {
    const name = req.query["name"];

    if (typeof name !== "string") {
      throw new BadRequestException(
        "name_is_not_string",
        "Name must be text"
      );
    }

    const message = await this.indexService.createTask({
      name: name
    });

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 201,
      response: response
    };
  });

  getTaskMilestones: RequestHandler = safeRequestHandler(async (req: Request) => {
    const taskId = parseInt(req.params["taskId"]);

    if (isNaN(taskId)) {
      throw new BadRequestException(
        "taskId_not_a_number",
        "Task Id must be a number"
      );
    }

    const entities = await this.indexService.getAllMilestonesForTask(taskId);

    const response = MilestoneModifiers.listEntityToResponse(entities);

    return {
      status: 200,
      response: response
    };
  });

  getCreateMilestone: RequestHandler = safeRequestHandler(async (req: Request) => {
    const taskId = parseInt(req.params["taskId"]);
    const name = req.query["name"];

    if (isNaN(taskId)) {
      throw new BadRequestException(
        "taskId_not_a_number",
        "Task Id must be a number"
      );
    }

    if (typeof name !== "string") {
      throw new BadRequestException(
        "name_is_not_string",
        "Name must be text"
      );
    }

    const message = await this.indexService.createMilestone({
      taskId: taskId,
      name: name
    });

    const response = MessageModifier.domainToResponse(message);

    return {
      status: 201,
      response: response
    };
  });

  getCreatePost: RequestHandler = safeRequestHandler(async (req: Request) => {
    let tags = req.query["tags"];
    const name = req.query["name"];

    if (typeof name !== "string") {
      throw new BadRequestException(
        "name_is_not_string",
        "Wrong name"
      );
    }

    let tagsArr: any[]
    if (!tags) {
      tagsArr = [];
    } else if (typeof tags === "string") {
      tagsArr = [tags];
    } else if (tags instanceof Array) {
      tagsArr = tags;
    } else {
      throw new BadRequestException(
        "tags_is_not_array_of_string",
        "Wrong tags"
      );
    }

    const tagCreations: TagCreation[] = tagsArr.map((t) => {
      if (typeof t !== "string") {
        throw new BadRequestException(
          "tags_is_not_array_of_string",
          "Wrong tags"
        );
      }
      return {
        name: t
      };
    });

    const message = await this.indexService.createPost({
      name: name
    }, tagCreations);

    const response: MessageResponse = MessageModifier.domainToResponse(message);

    return {
      status: 201,
      response: response
    };
  });

  getPosts: RequestHandler = safeRequestHandler(async (req: Request) => {
    const postEntities = await this.indexService.getPosts();

    const response: PostResponse[] = PostModifers.listEntityToResponse(postEntities);

    return {
      status: 200,
      response: response
    };
  });

}
