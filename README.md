# About
This is example of basic nodejs server writen in typescript with mariadb and redis with express router.

# Development and Deployment 
There is available basic solutions with docker-compose for development and deployment. Also there is templates for debbuging inside docker container for VSCode and Neovim

# CI/CD
There is basic pipelines available for gitlab-ci and Jenkins
